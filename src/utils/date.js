export const renderDate = text => {
  const date = new Date(text)
  const today = new Date()
  if(today.getMonth() !== date.getMonth()) {
    return `${date.getFullYear()}/${date.getMonth()}/${date.getDate()}`
  } else if (today.getDate() - date.getDate() === 1) {
    return `${date.getMonth()} : ${date.getDate()}`
  }
  return `${date.getHours()} : ${date.getMinutes()}`
}
