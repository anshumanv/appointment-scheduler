export const UserInfo = {
  name: 'Professional Name',
  uid: 1,
  title: 'Psychologist',
  location: 'Lisbon',
  rating: 5,
  rating_count: 20,
  rate: 50,
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  image: 'https://github.com/anshumanv.png'
}

export const availableTimeSlots = id => {
  // date: available slots
  return {
    '20': '8-10',
    '21': '8-10',
    '22': '8-10'
  }
}