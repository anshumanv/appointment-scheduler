import React, { useState, useEffect } from 'react';
// import DatePicker from '../components/RangePicker'
import UserCard from '../components/UserCard'
import { UserInfo } from '../utils/mock_data'
import TimeslotPicker from '../components/TimeslotPicker'


function App() {
  const [user, setUser] = useState({})
  console.log(user)
  useEffect(() => {
    setUser(UserInfo)
    return () => {
      // cleanup
    };
  }, [])
  return (
    <div className="App">
      <UserCard user={user} />
      <TimeslotPicker />
      {/* <DatePicker setDateFilter={setDateFilter} /> */}
      {/* <div className="results">Results: <span>{mailData.length}</span> mail(s)</div>
      <Divider />
      <div className="table-wrapper">
        <MailList mails={mailData} columns={columns} />
      </div> */}
    </div>
  );
}

export default App;
