const app = require('express')();
const cors = require('cors')

app.use(cors())

app.get('/', (req, res) => {
  const data = {
    '20': ['08:00', '08:30', '09:00', '09:30', '10:00'],
    '21': ['08:00', '08:30', '09:00', '09:30', '10:00'],
    '22': ['08:00', '08:30', '09:00', '09:30', '10:00'],
  }
  res.json(data)
})

app.listen(3001, () => console.log('listening on port 3001'))
