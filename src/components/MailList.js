import React from 'react'
import { Table, List } from 'antd';
import ListItem from './ListItem'
import logo from '../assets/logo.png'


const MailList = ({ mails, columns }) => {
  return <>
    {
      mails.length !== 0 ? (
        <>
          <div className="mail-table">
            <Table columns={columns} dataSource={mails} />
            }
          </div>
          <div className="mail-list">
            <List
              dataSource={mails}
              renderItem={item => (
                <List.Item><ListItem mail={item} /></List.Item>
              )} 
            />
          </div>
        </>
      ): <img src={logo} alt="logo"/>
    }
  </>
}

export default MailList
