import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Icon } from 'antd'
import '../styles/timeslot.scss'

const renderSlots = range => {
  console.log(range)
  const cells = range.map(time => {
    return <span className="slot-times-cell">{time}</span>
  })
  cells.push(<span className="slot-times-cell">MORE</span>)
  return <div className="slot-times-column">
    {cells}
  </div>
}

const TimeslotPicker = () => {
  const [slots, setSlots] = useState({})
  useEffect(() => {
    axios.get('http://localhost:3001').then(slots => setSlots(slots.data))
    return () => {
      // cleanup
    };
  }, [])
  const dates = Object.keys(slots)
  console.log(dates)
  const times = Object.values(slots)
  return <>
    <div className="slot">
      <div className="slot-header">
        <div className="slot-title">
          <span className="slot-title-heading">Schedule your session!</span>
          <span>Timezone: Lisbon (+1)</span>
        </div>
        <div className="slot-dates">
          <Icon type="left-circle" />
          {dates.map(date => (
            <span key={date}>AUG {date}</span>
          ))}
          <Icon type="right-circle" />
        </div>
      </div>
      <div className="slot-times">
        {dates.map(date => renderSlots(slots[date]))}
      </div>
    </div>
  </>
}

export default TimeslotPicker
