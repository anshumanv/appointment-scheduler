import React from  'react';
import { Rate } from 'antd';
import '../styles/user.scss'

const UserCard = ({ user }) => {
  console.log(user)
  return (
    <>
      <div className="user-card">
        <div className="user-card-top">
          <img src={user.image} alt=""/>
          <div className="user-card-info">
            <span className="user-card-name">{user.name}</span>
            <span>
              <span className="user-card-title">{user.title}</span> <span>| {user.location}</span>
            </span>
            <span><Rate allowHalf disabled value={user.rating} /> ({user.rating_count} reviews)</span>
            <span>R${user.rate} / 50 MINUTES</span>
          </div>
        </div>
        <div className="user-card-bottom">
          {user.description}
        </div>
      </div>
    </>
  )
}

export default UserCard;
