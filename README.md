# Appointment Scheduler

### Development 

* Made a quick server to mock HTTP request for the scheduler.

Install Deps
```sh
npm i
```

OR

```sh
yarn install
```

Start Serter

```sh
npm run server
```

Start frontend from project root

```sh
npm start
```

Uses?

* React with Hooks
* antd - Design system
* axios - HTTP client
* SASS - Style preprocessor

